import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

df = pd.read_csv('kyphosis.csv')

print(df.head(n=10))

from sklearn.model_selection import train_test_split

X = df.drop('Kyphosis',axis=1)
y = df['Kyphosis']
# 70 percent training 
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.30)

#Decision Trees
print()
print("decision tree outcome")
from sklearn.tree import DecisionTreeClassifier
dtree = DecisionTreeClassifier()

dtree.fit(X_train,y_train)

predictions = dtree.predict(X_test)

from sklearn.metrics import classification_report,confusion_matrix

print(classification_report(y_test,predictions))

# print(confusion_matrix(y_test,predictions))

#Random Forest
print()
print("random forest outcome")
from sklearn.ensemble import RandomForestClassifier
# n_estimators = nmber of tree in the forest
rfc = RandomForestClassifier(n_estimators=100)
rfc.fit(X_train, y_train)

rfc_pred = rfc.predict(X_test)

# print(confusion_matrix(y_test,rfc_pred))

print(classification_report(y_test,rfc_pred))

